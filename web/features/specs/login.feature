#language: pt

Funcionalidade: Login

    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "favero.test@gmail.com" e "pwd123"
        Então sou redirecionado para o Dashboard

    
    Esquema do Cenario: Tentativa de login
        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<messagem_output>"

        Exemplos:
            | email_input           | senha_input | messagem_output                  |
            | favero.test@gmail.com | abc123      | Usuário e/ou senha inválidos.    |
            | jarbas@bol.com.br     | pwd123      | Usuário e/ou senha inválidos.    |
            | favero.test&gmail.com | pwd123      | Oops. Informe um email válido!   |
            |                       | pwd123      | Oops. Informe um email válido!   |
            | favero.test@gmail.com |             | Oops. Informe sua senha secreta! |

